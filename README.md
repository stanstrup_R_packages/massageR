# massageR
Functions for data massaging in R


## Functions
* `group_correct`: Batch-correct dataset feature-wise.
* `rep_na`: Replacement of NA values column-wise. Options can instruct the function to replace by half the minimum value or by a random value between zero and the minimum non-NA value. Custom functions can be supplied for the replacement.
* Normalize between 0 and 1 (`normalize01`) or between any two numbers (`normalize_range`).
* `fold.change`: (fast) Fold change calculation between all groups of samples.
* `heat.clust`: Prepare dendrograms for gplots' heatmap.2. The type of scaling can be adjusted and is performed before dendrogram calculations (as opposed to native heatmap.2!!!), reordering can be turned on/off and distance and clustering functions can be customized.
* `diff_mat2letter_not`, `edges2diff_mat`: Letter notation for overlapping confidence intervals familiar in multiple comparisons testing. edges2diff.mat turns upper and lower bounds of confidence intervals into a logical matrix indicating which entries overlap. diff.mat2letter.not creates the letter notation.

## Installation
Install the devtools package and run:

```r
devtools::install_git("https://gitlab.com/R_packages/massageR.git")
```
