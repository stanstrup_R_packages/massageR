##' @rdname normalize_range
##' @export

normalize01 <- function(v){
  m <- min(v)
  range <- max(v) - m
  v <- (v - m) / range
  return(v)
}

##' Data normalization
##' 
##' Normalize data between 0 and 1 or between two choosen values.
##' 
##' 
##' @aliases normalize01 normalize_range
##' @param v Vector to normalize
##' @param x Lower value of the range to normalize to.
##' @param y Upper value of the range to normalize to.
##' @return Normalized vector.
##' @author Original code by Max Sipos (see reference). Converted from matlab
##' to R code and added to package by Jan Stanstrup,
##' \email{stanstrup@@gmail.com}
##' @references %% ~put references to the literature/web site here ~
##' http://stackoverflow.com/questions/10364575/normalization-in-variable-range-x-y-in-matlab
##' @export
##' 

normalize_range <- function(v, x, y){
  # Normalize to [0, 1]:
  v <- normalize01(v)
  
  # Then scale to [x,y]:
  range2 <- y - x
  normalized <- (v*range2) + x
  
  return(normalized)
}

