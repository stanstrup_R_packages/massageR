
#' @rdname is_between
#' @export
#' 

is_between_1range <- function(x,a,b) {
    
    if(!is.na(a) & !is.na(b)) return(x>=a & x<=b)
    if(is.na(a) & is.na(b))   return(rep(TRUE,length(x))) 
    if(is.na(a)) return(x<=b)
    if(is.na(b)) return(x>=a)
    
}

#' is_between
#' 
#' 
#'
#' @param x numeric vector to check.
#' @param a lower limit of interval. Scalar for is_between_1range. Can be a vector for is_between.
#' @param b upper limit of interval. Scalar for is_between_1range. Can be a vector for is_between.
#'
#' @return Logical vector (is_between_1range) of length x or 
#' logical matrix (is_between) of dimensions x times length of a (== length of b).
#' @export
#'

is_between <- function(x,a,b) {
    
    apply(cbind(a,b),1,function(y) is_between_1range(x,y[1],y[2]))
    
}
