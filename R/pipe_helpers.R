#' Remove attribute of an object in a pipe friendly way
#'
#' @param x The object to remove attribute from
#' @param which name of the attribute to remove
#'
#' @return "x" without attribute "which"
#' @export
#'

attr_rem <- function(x, which) {
    attr(x, which) <- NULL
    return(x)
}
